import React from 'react';
import { INCREMENT,DECREMENT } from '../containers/CounterContainer/constant';

function Home({
  onIncrement,
  onDecrement,
  counter,
}) {
  return (
    <div>
      <button onClick={() =>onIncrement(INCREMENT)}>
       Incrementer
      </button>
      <button onClick={() =>onDecrement(DECREMENT)}>
        Decrementer
      </button>
      <span>
        <h1 style={
          {
            border: '2px solid red',
            "text-align": 'center',
            width: 200
          }
        }>{counter}</h1>
      </span>
    </div>
  );
};

export default Home;
